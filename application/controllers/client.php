<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('structure_controller.php');
class Client extends StructureController {

	
	public function index()
	{
		$this->page = 'dashboard';
		$this->layout();
		
		
	}

	public function search()
	{

		$search = $this->input->post('search_term');
		if(!empty($search))
		{
		$results = $this->CommonFunctions->search($search);
		$this->data['results'] = $results;
		$this->data['search'] = $search;
		
	}

		$this->page = 'search';
		$this->layout();
	}
	
}
