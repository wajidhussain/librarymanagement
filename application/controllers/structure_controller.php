<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StructureController extends CI_Controller {

	protected $data = array();
	public function __construct()
	{
		parent::__construct();
  
	}
	public function layout()
	{

	$this->files['header'] = $this->load->view('includes/header', $this->data, true);
	$this->files['footer'] = $this->load->view('includes/footer', $this->data, true);
	$this->files['sidebar'] = $this->load->view('includes/sidebar', $this->data, true);
	$this->files['page'] = $this->load->view($this->page, $this->data, true);
	$this->load->view('includes/loadfiles', $this->files);
	}
	
	public function authUser(){
		$sess_id = $this->session->userdata('userdata');
		if(!empty($sess_id)){
			return $sess_id[0]['role'];
		}
		else{
			return false;
		}
	}
}
