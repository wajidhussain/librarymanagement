<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('structure_controller.php');
class Admin extends StructureController {

	
	public function index()
	{
		if($this->session->userdata('role') == 'admin')
		{
			$this->page = 'dashboard';
			$this->layout();
		}
		else
		{
			$this->page = 'error';
			$this->layout();
		}
		
		
	}
	
}
