<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('structure_controller.php');
class Books extends StructureController {

	
	public function index()
	{

		$Books = $this->CommonFunctions->getAllBooks();
		$Racks = $this->CommonFunctions->get_all('racks');
		$this->data['books']  = $Books;
		$this->data['racks']  = $Racks;
		$this->page = 'booklist';
		$this->layout();
		
		
	}
	function view($id = '')
	{
		if($this->session->userdata('role') == 'admin')
		{
			$book = $this->CommonFunctions->getSingleRecord('books' , array('book_id' => $id));
			$Racks = $this->CommonFunctions->get_all('racks');
			$this->data['racks']  = $Racks;
			$this->data['book']  = $book;
			$this->page = 'book_form';
			$this->layout();
		}
		else
		{
			$this->page = 'error';
			$this->layout();
		}
	}
	public function bookexist($bookName){

		$book = $this->CommonFunctions->getSingleRecord('books' , array('book_title' => $bookName));
		if(count($book) > 0){
			return true;
		}
		else{
			return false;
		}
	}
	public function checkQuantity($rackID){

		$books = $this->CommonFunctions->countResults('books' , array('rack_id' => $rackID));
		
		if($books <= 10){
			return true;
		}
		else{
			return false;
		}
	}
	public function save()
	{
	if($this->session->userdata('role') == 'admin')
	{
		$book = $this->input->post();
		extract($book);
		if($this->input->post('book_id')){

				$bookupdate = array('book_title' => $bookname, 
					  'author' => $author, 
					  'published_year' => $published_year, 
					  'rack_id' => $rack_id
					 );
				if($this->checkQuantity($rack_id)){
					$this->CommonFunctions->update('books', $bookupdate , array('book_id' => $book_id));
					$this->session->set_flashdata('message', 'Book updated successfully');
					$this->session->set_flashdata('status', 'success');
				}
				else{
					$this->session->set_flashdata('message', 'Sorry, you cannot add more than 10 books!');
					$this->session->set_flashdata('status', 'danger');
				}
				
			
			}
			else{

					if($this->checkQuantity($rack_id)){
						$bookInsert = array('book_title' => $bookname, 
						  'author' => $author, 
						  'published_year' => $published_year, 
						  'rack_id' => $rack_id
						 );
		 				$this->CommonFunctions->insert($bookInsert , 'books');
						$this->session->set_flashdata('message', 'Book added successfully');
						$this->session->set_flashdata('status', 'success');
					}
					else{
						$this->session->set_flashdata('message', 'Sorry! you cannot add more than 10 books in this rack' );
						$this->session->set_flashdata('status', 'danger');
					}
					
			
			}
		
		redirect('books');
	}
	else
	{
		$this->page = 'error';
		$this->layout();
	}
}
	
	public function deletebook(){

		$book_id = $this->input->post('book_id');
		$book = $this->CommonFunctions->delete('books' , array('book_id' => $book_id));
		$this->session->set_flashdata('message', 'Book deleted successfully!');
		$this->session->set_flashdata('status', 'success');
		redirect('books');
		
	}
	public function getBookData(){
		$rackid = $this->input->post('rack_id');
		$books = $this->CommonFunctions->getSingleRecord('books' , array('rack_id' => $rackid));
		echo json_encode($books);
	}

	
}
