<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('structure_controller.php');
class Racks extends StructureController {

	
	public function index()
	{

		$records = $this->CommonFunctions->get_all('racks');
		$racks=$this->CommonFunctions->getRack();
		$this->data['racks']  = $racks;
		$this->page = 'racklist';
		$this->layout();
		
		
	}
	public function rackexist($rackName){

		$rack = $this->CommonFunctions->getSingleRecord('racks' , array('rack_name' => $rackName));
		if(count($rack) > 0){
			return true;
		}
		else{
			return false;
		}
	}
	function view($id = '')
	{
		if($this->session->userdata('role') == 'admin')
		{
			$reck = $this->CommonFunctions->getSingleRecord('racks' , array('rack_id' => $id));
			$this->data['rack']  = $reck;
			$this->page = 'rack_form';
			$this->layout();
		}
		else
		{
			$this->page = 'error';
			$this->layout();
		}
	}
	
	public function save()
	{
	if($this->session->userdata('role') == 'admin')
		{
		$rackName = $this->input->post('rackname');

		if(!$this->rackexist($rackName)){

			if($this->input->post('rack_id')){

				$rackId = $this->input->post('rack_id');
				$this->CommonFunctions->update('racks', array('rack_name' => $rackName) , array('rack_id' => $rackId));
				$this->session->set_flashdata('message', 'Rack updated successfully');
				$this->session->set_flashdata('status', 'success');
			
			}
			else{

				$this->CommonFunctions->insert(array('rack_name' => $rackName) , 'racks');
				$this->session->set_flashdata('message', 'Rack added successfully');
				$this->session->set_flashdata('status', 'success');
			
			}
		}
		else{

			$this->session->set_flashdata('message', 'Rack already exits!');
			$this->session->set_flashdata('status', 'danger');
		
		}
		redirect('racks');
	}
	else
	{
		$this->page = 'error';
		$this->layout();
	}
}
	
	public function deleterack(){

		$rack_id = $this->input->post('rack_id');
		$rack = $this->CommonFunctions->delete('racks' , array('rack_id' => $rack_id));
		$this->session->set_flashdata('message', 'Rack deleted successfully!');
		$this->session->set_flashdata('status', 'success');
		redirect('racks');
		
	}

	
	
}
