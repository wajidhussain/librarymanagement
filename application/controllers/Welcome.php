<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('structure_controller.php');
class Welcome extends StructureController {


	
	public function index()
	{
		$isloggedin = $this->authUser();
		if(!$isloggedin){
			$this->page = 'home';
			$this->layout();
		}
		else{

			redirect(base_url($isloggedin));
		}
		
		
	}
	
	public function authentication(){
		$userData = $this->input->post();

		extract($userData);
		$userCheck = $this->UserModel->authenticateUser($username, md5($password));
		if(count($userCheck) > 0){
			$this->session->set_userdata('userdata', $userCheck);
			$this->session->set_userdata('role', $userCheck[0]['role']);
			redirect(base_url('welcome'));
			}
		else{
			$this->session->set_flashdata('message', 'Invalid username or password!');
			$this->session->set_flashdata('status', 'danger');
			$this->page = 'home';
			$this->layout();
			
		}		
	}
	public function logout(){
		$this->session->unset_userdata('userdata');
		redirect(base_url('welcome'));
	}
}
