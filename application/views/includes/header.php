<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Library Management System</title>

    <link href="<?php echo base_url('public/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('public/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('public/css/style.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('public/js/jquery-3.1.1.min.js') ?>"></script>

</head>

<body>
