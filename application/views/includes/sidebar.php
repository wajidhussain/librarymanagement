<div id="wrapper">
<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
            <?php if($this->session->userdata('userdata') != '' ) { ?>

                <ul class="nav metismenu" id="side-menu">
                    
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" src="<?php echo base_url('public/img/profile_small.jpg')?>" />
                                 </span>
                        </div>
                        
                    </li>
                   <?php if($this->session->userdata('role') == 'admin'){ ?>
                    <li class="active">
                        <a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Admin Dashboard</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Racks</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url()?>racks">Racks List</a></li>
                            <li><a href="<?php echo base_url()?>racks/view">Add Rack</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Books</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url()?>books">Books List</a></li>
                            <li><a href="<?php echo base_url()?>books/view">Add Book</a></li>
                        </ul>
                    </li>
                <?php } 
                    else{
                        ?>
                    <li class="active">
                        <a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> <span class="nav-label">
                        Client Dashboard</span></a>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Racks</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url()?>racks">Racks List</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;"><i class="fa fa-users"></i> 
                            <span class="nav-label">Search</span> 
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url()?>client/search">Search Book</a></li>
                        </ul>
                    </li>
                    <?php }?>
                
                </ul>
                <?php 
                     }
                else{ ?>
                    <nav class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav metismenu" id="side-menu">
                            <li class="nav-header">
                                <div class="dropdown profile-element"> 
                                </div>
                                <div class="logo-element">
                                   Admin
                                </div>
                            </li>
                            <li class="active">
                                <a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> <span class="nav-label">Welcome</span></a>
                            </li>
                            </ul>
                        </div>
                    </nav>
                <?php } ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        
            <ul class="nav navbar-top-links navbar-right">
                
               
                <div class="top-right links">
                    <br>
               
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                   
                </form>
                   <?php if(!empty($this->session->userdata('userdata'))){
                    echo '<li ><a href="'.base_url('welcome/logout').'">Logout</a></li>';
                        
                    }
                    else{
                        echo '<li ><a href="'.base_url().'">Login</a></li>';
                       
                    }
                    ?>
                        
                </div>
               
            </ul>

        </nav>
        </div>