<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Search Book</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" action="<?php echo base_url('client/search'); ?>"
                 enctype="multipart/form-data">
                 
                    <div class="form-group row ">
                      <label class="col-sm-2 col-form-label">Search <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" name="search_term" class="form-control" value="<?php echo isset($search)?$search:''?>">
                          
                        </div>
                    </div> 
                    <div class="hr-line-dashed"></div>
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit"> Search
                            </button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
</div>
<?php if(isset($results)){ ?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Search Results</h5>
                <div class="ibox-tools">
                    
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Book Title</th>
                            <th>Author</th>
                            <th>Published Date</th>
                            <th>Rack Name</th>
                        </tr>
                        </thead>
                        <tbody>

                            <?php
                            if(count($results) > 0){
                                foreach($results as $k=>$result){ ?>
                                    <tr>
                                        <td><?php echo $k+1?></td>
                                        <td><?php echo $result->book_title?></td>
                                        <td><?php echo $result->author?></td>
                                        <td><?php echo $result->published_year?></td>
                                        <td><?php echo $result->rack_name?></td>
                                    </tr>  
                                <?php
                                    }
                                }
                                else{
                                    ?> 
                                <tr>
                                    <td colspan="5">No Record Found!</td>
                                </tr>
                                <?php }?>                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>