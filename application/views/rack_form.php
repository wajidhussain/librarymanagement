<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php 
                        if($this->uri->segment(3) == ''){?>
                            Add Rack
                            <?php }
                            else{?>
                            Edit Rack
                            <?php }?>
                                
                            </h5>
            </div>
           
            <div class="ibox-content">
              
                <form method="POST" action="<?php echo base_url('racks/save')?>" enctype="multipart/form-data">
               
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Rack Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                      
                            <input type="text" required="required" name="rackname" class="form-control" value="<?php echo isset($rack[0]->rack_name)?$rack[0]->rack_name:'';?>">
                          
                        </div>
                    </div> 
                    <input type="hidden" name="rack_id" value="<?php echo isset($rack[0]->rack_id)?$rack[0]->rack_id:'';?>">

                                            
                    <div class="hr-line-dashed"></div>
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">
                                Save</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
