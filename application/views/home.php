<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <br><br>
                    <?php if($this->session->flashdata('status') !== null){ 
                                $status = $this->session->flashdata('status');
                                $message = $this->session->flashdata('message');
                                 echo '<div class="alert alert-'.$status.'" style="">'.$message.'<button type="button" class="close" data-dismiss="alert">×</button></div>';
                    }?>
                    <form method="POST" action="<?php echo base_url('welcome/authentication');?>">
                  
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right"> Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="username" value="" required autofocus>

                               
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                   Login
                                </button>

                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>