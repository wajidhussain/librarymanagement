<div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Racks List</h5>
                            <div class="ibox-tools">
                                
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php if($this->session->flashdata('status') !== null){ 
                                $status = $this->session->flashdata('status');
                                $message = $this->session->flashdata('message');
                                 echo '<div class="alert alert-'.$status.'" style="">'.$message.'<button type="button" class="close" data-dismiss="alert">×</button></div>';
                            }?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover list-dataTable" >
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Rack Name</th>
                                        <th>
                                        <?php if($this->session->userdata('role') == 'admin') { ?>
                                        Actions
                                        <?php }else{?>
                                        Total Books
                                        <?php }?>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(count($racks) > 0){  
                                          foreach($racks as $k=>$rack) { ?>
                                            <tr>
                                                <td><?php echo $k+1?></td>
                                                <td><?php echo $rack->rack_name; ?></td>
                                                <td>
                                                    <?php if($this->session->userdata('role') == 'admin') { ?>

                                                            <a href="<?php echo base_url()?>racks/view/<?php echo $rack->rack_id;?>" class="editRack btn btn-primary" rid="<?php echo $rack->rack_id;?>">Edit</a> 
                                                            <a href="javascript:void(0);" class="delete btn btn-danger" rack_id="<?php echo $rack->rack_id;?>">Delete</a>
                                                            <?php } else { ?>
                                                            
                                                            <a href="javascript:void(0);" class="detail" data-toggle="modal" rack_id = "<?php echo $rack->rack_id;?>">View Detail(<?php echo $rack->totalBooks; ?>)</a>
                                                    <?php } ?>

                                                </td>
                                            </tr>
                                        <?php }}
                                        else{ ?>
                                         <tr>
                                            <td colspan="6">No Record Found!</td>
                                        </tr>
                                    <?php }?>
                                                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url('racks/deleterack'); ?>" method="post" id="deleteForm">
    <input type="hidden" id="rack_del" name="rack_id" />
</form>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Book Detail</h4>
        </div>
        <div class="modal-body">
          <table class="table table-striped table-bordered table-hover" >
              <th>Book Title</th>
              <th>Author</th>
              <th>Published</th>
              <tbody id="menu">
                  
              </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>


$('.delete').click(function(event) {
    if(confirm("Are you sure you want to delete?")) {
       $('#rack_del').val($(this).attr('rack_id'));
        $("#deleteForm").submit();  
    } 
});

$('.detail').click(function(event) {
    var rack_id = $(this).attr('rack_id');
         $.ajax({
             type:"POST",
             url:"<?php echo base_url('books/getBookData')?>",
             dataType:"json",
             data:{"rack_id":rack_id},
             success: function(d)
             {
                $('#menu').html('');
                console.log(d);
                if(d != '')
                {
                   $.each( d, function( key, value ) {
                       $('#menu').append('<tr><td>'+value.book_title+'</td><td>'+value.author+'</td><td>'+value.published_year+'</td></tr>');
                    });
                   $('#myModal').modal('show');
                  }
                else
                {
                    $('#menu').append('<tr><td colspan="3">No record found</td></tr>');
                    $('#myModal').modal('show');
                }

             },
             error: function(e)
             {
                $('#myModal').modal('hide');
                 alert('something went wrong');

             }
        });
});


</script>