<div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Books List</h5>
                            
                        </div>
                        <div class="ibox-content">
                            <?php if($this->session->flashdata('status') !== null){ 
                                $status = $this->session->flashdata('status');
                                $message = $this->session->flashdata('message');
                                 echo '<div class="alert alert-'.$status.'" style="">'.$message.'<button type="button" class="close" data-dismiss="alert">×</button></div>';
                            }?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover list-dataTable" >
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Published Year</th>
                                        <th>Rack</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($books) > 0){ 
                                        foreach($books as $k=>$book) { ?>
                                        <tr>
                                            <td><?php echo $k+1?></td>
                                            <td><?php echo $book->book_title; ?></td>
                                            <td><?php echo $book->author; ?></td>
                                            <td><?php echo $book->published_year; ?></td>
                                            <td><?php echo $book->rack_name; ?></td>
                                            <td>
                                              <a href="<?php echo base_url()?>books/view/<?php echo $book->book_id;?>" class="editRack btn btn-primary" >Edit</a> 
                                               <a href="javascript:void(0);" class="delete btn btn-danger" book_id="<?php echo $book->book_id;?>">Delete</a>
                                            </td>
                                        </tr>
                                    <?php }}
                                    else{ ?>
                                    <tr>
                                        <td colspan="6">No Record Found!</td>
                                    </tr>
                                    <?php }?>
                                                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo base_url('books/deletebook'); ?>" method="post" id="deleteForm">
    <input type="hidden" id="book_del" name="book_id" />
</form>
<script>

$('.delete').click(function(event) {
    if(confirm("Are you sure you want to delete?")) {
       $('#book_del').val($(this).attr('book_id'));
        $("#deleteForm").submit();  
    } 
});

</script>