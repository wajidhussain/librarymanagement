<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php 
                        if($this->uri->segment(3) == ''){?>
                            Add Book
                            <?php }
                            else{?>
                            Edit Book
                            <?php }?>
                                
                            </h5>
            </div>
           
            <div class="ibox-content">
              
                <form method="POST" action="<?php echo base_url('books/save')?>" enctype="multipart/form-data">
               
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Book Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required="required" name="bookname" class="form-control" value="<?php echo isset($book[0]->book_title)?$book[0]->book_title:'';?>">
                        </div>
                    </div> 
                    <input type="hidden" name="book_id" value="<?php echo isset($book[0]->book_id)?$book[0]->book_id:'';?>">

                     <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Author <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" required="required" name="author" class="form-control" value="<?php echo isset($book[0]->author)?$book[0]->author:'';?>">
                        </div>
                    </div> 
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Published Year <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" required="required" name="published_year" class="form-control" value="<?php echo isset($book[0]->published_year)?$book[0]->published_year:'';?>">
                        </div>
                    </div> 
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Select Rack <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control" name="rack_id" required="required">
                                <option value="">Select Rack</option>
                                <?php foreach ($racks as $rack) {
                                     $s=($book[0]->rack_id==$rack->rack_id) ? 'selected':'';
                                    echo '<option '.$s.' value="'.$rack->rack_id.'">'.$rack->rack_name.'</option>';
                                } ?>
                            </select>
                        </div>
                    </div> 
                                            
                    <div class="hr-line-dashed"></div>
                
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">
                                Save</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
