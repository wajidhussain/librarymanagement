<?php

class CommonFunctions extends CI_Model {

   public function insert($data , $tbl_name){

       return  $this->db->insert($tbl_name , $data);

   }
   public function getAllBooks(){
         return $this->db->from('books')
        ->join('racks', 'books.rack_id = racks.rack_id', 'inner')
        ->get()
        ->result();
   }
   public function getBook($bookId){
         return $this->db->from('books')
        ->join('racks', 'books.rack_id = racks.rack_id', 'inner')
        ->where('book_id' , $bookId)
        ->get()
        ->result();
   }  
   public function getRack()
   {

    $query = $this->db->query("SELECT racks.rack_id, max(racks.rack_name) as rack_name ,COUNT(books.book_title) as totalBooks FROM books RIGHT JOIN racks on racks.rack_id=books.rack_id GROUP by racks.rack_id")->result();
    return $query;
    
   }
   public function get_all($tbl_name){

        return $this->db->get($tbl_name)->result();

   }
   public function getSingleRecord($tbl_name, $condition){

        return $this->db->get_where($tbl_name, $condition)->result();

   }
   public function update($table, $data, $condition){

    $this->db->set($data)
    ->where($condition)
    ->update($table); 

   }
   public function delete($table, $condition){

    $this->db->where($condition)
    ->delete($table);

   }
   public function countResults($table, $condition){

    return $this->db->where($condition)
    ->from($table)
    ->count_all_results();
   }

   public function search($search){       
      
        $data = $this->db->query("SELECT * from books b inner join racks on racks.rack_id = b.rack_id WHERE   b.book_title LIKE '%$search%' OR b.author LIKE '%$search%' OR b.published_year LIKE '%$search%'");
        
        return $data->result();

   }
   
	
}

?>